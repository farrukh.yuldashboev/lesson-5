    //Helper functions
function sleep(time) {
    return new Promise((resolve) => setTimeout(resolve, time))};

    // lesson 5
function countVowels (str) {
    var vowelsList = ["a", "o", "u", "e", "i"]
    vowelsList.forEach(element => vowelsList.push(element.toUpperCase()));
    var countNum = 0
    for (let i = 0; i < str.length; i++) {
        for (let j = 0; j < str.length; j++) {
            if (str[i] === vowelsList[j]) {
                countNum++
            }
        }
    }
    console.log(countNum);
};

function calculateLeftDaysToNewYear (date = new Date()){
    const MS_PER_DAY = 1000 * 60 * 60 * 24;
    const startDay = new Date(date)
    const newYear = new Date("2022-01-01")
    //Discarding the time and time-zone information
    const utc1 = Date.UTC(startDay.getFullYear(), startDay.getMonth(), startDay.getDate());
    const utc2 = Date.UTC(newYear.getFullYear(), newYear.getMonth(), newYear.getDate());
    const output = Math.floor((utc2 - utc1) / MS_PER_DAY)

    if (output < 0) {
        console.log(`${Math.abs(output)} days passed from 2021 New Year`);
    }else {
        console.log(`${output} days left until 2021 New Year`);
    }
};

function findSymbols (arr) {
    var symbolsList = ["~", "!", "@", '#', "$", "%", "^", "&", "*"]
    var outArr = []
    arr.forEach(elem => {
        for (char of elem) {
            symbolsList.forEach(item => {
                if (char == item) {
                    outArr.push(char)
                }
            })
        }
    })
    console.log(outArr);

};


function findIntersection (arr1, arr2) {
    var intersectionList = []
    arr1.forEach(item1 => {
        arr2.forEach(item2 => {
            if (item1 === item2) {
                intersectionList.push(item1)
            }
        })
    })
    console.log(intersectionList);
};

function sleepAlert(time) {
    sleep(time).then(() => {
        alert(`You waited ${time} milliseconds`)
    })
};

function sumNaturalNum(arr) {
    var sumNatural = 0
    arr.forEach(elem => {
        if (Number.isInteger(elem)) {
            sumNatural += elem
        }
    })
    console.log(sumNatural);
};

function countCharOccurance (str, char) {
    var charOccurance = 0
    for (item of str) {
        if (item === char) {
            charOccurance++
        }
    }
    console.log(charOccurance);
};

countVowels('The quick brown fox')
calculateLeftDaysToNewYear() // by DEFAULT today is the start date
                                // //enter date in format "year-month-day" for a start day => "2021-10-17"
findSymbols(["@k", "$", "h", "a", "in thhis string there is& and *^ and !"])
findIntersection([1,2,3,4], [2,3,4,5])
// sleepAlert(4000)   // Please test in browser and comment out when checked!
sumNaturalNum([1, 2, 3, 4, 0.5])
countCharOccurance("there was message which had sent by my side", "y")
